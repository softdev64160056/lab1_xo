/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.xo;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Xo {

    static char[][] Table = {{'-', '-', '-',}, {'-', '-', '-',}, {'-', '-', '-',},};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;

    public static void main(String[] args) {
        printStart();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();

            if (isWin()) {
                printTable();
                printWin();

            }
            if (isDraw()) {
                printTable();
                printDraw();

            }
            switchPlayer();

        }

    }

    private static void printStart() {
        System.out.println("Start XO Game!!!");
    }

    private static void printTurn() {
        System.out.println("Turn Player: " + currentPlayer);
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (Table[row - 1][col - 1] == '-') {
                Table[row - 1][col - 1] = currentPlayer;
                return;
            }

        }

    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(Table[i][j]);
            }
            System.out.println("");
        }
    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if ((checkRow() || checkCol()) || checkX1() || checkX2()) {
            return true;
        } else {
        }
        return false;
    }

    private static void printWin() {
        System.out.println(currentPlayer + " Win!!!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (Table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int j = 0; j < 3; j++) {
            if (Table[j][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;

    }

    private static boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (Table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (Table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (Table[i][j] == '-') {
                    return false;
                }
            }

        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Draw!!!");
    }
}
